#include "Add.h"

Add::Add(){}

Add::Add(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "ADD", dataType, componentID){
	setName("add" + componentID);
}

Add::Add(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "ADD", dataType, componentID){
	setName("add" + componentID);
}

void Add::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Add::getInputOne(){
	return inputOne;
}

void Add::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Add::getInputTwo(){
	return inputTwo;
}

void Add::setOutput(string output){
	this->output = output;
}

string Add::getOutput(){
	return output;
}

void Add::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Add::getInputWidth(){
	return Component::getInputWidth();
}

void Add::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Add::getOutputWidth(){
	return Component::getOutputWidth();
}

void Add::setDelay(float delay){
	Component::setDelay(delay);
}

float Add::getDelay(){
	return Component::getDelay();
}

void Add::setName(string name){
	Component::setName(name);
}

string Add::getName(){
	return Component::getName();
}

void Add::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Add::getComponentID(){
	return Component::getComponentID();
}

void Add::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Add::getComponentType(){
	return Component::getComponentType();
}

void Add::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Add::getDataType(){
	return Component::getDataType();
}

void Add::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Add::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Add::generateVerilogExpression(){

}

Add::~Add(){

}