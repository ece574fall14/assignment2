#include "Port.h"

Port::Port(){}

Port::Port(string name, int width, string portType, string dataType, int componentID) :
 Component(width, width, PORT_DELAY, name, "PORT", dataType, componentID){}

void Port::setName(string name){
	Component::setName(name);
}

string Port::getName(){
	return Component::getName();
}

void Port::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Port::getInputWidth(){
	return Component::getInputWidth();
}

void Port::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Port::getOutputWidth(){
	return Component::getOutputWidth();
}

void Port::setDelay(float delay){
	Component::setDelay(delay);
}

float Port::getDelay(){
	return Component::getDelay();
}

void Port::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Port::getComponentID(){
	return Component::getComponentID();
}

void Port::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Port::getComponentType(){
	return Component::getComponentType();
}

void Port::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Port::getDataType(){
	return Component::getDataType();
}

void Port::setPortType(string portType){
	this->portType = portType;
}

string Port::getPortType(){
	return portType;
}

void Port::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Port::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Port::generateVerilogExpression(){

}

Port::~Port(){

}