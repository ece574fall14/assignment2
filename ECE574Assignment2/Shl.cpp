#include "Shl.h"

Shl::Shl(){}

Shl::Shl(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "SHL", dataType, componentID){
	setName("shl" + componentID);
}

Shl::Shl(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "SHL", dataType, componentID){
	setName("shl" + componentID);
}

void Shl::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Shl::getInputOne(){
	return inputOne;
}

void Shl::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Shl::getInputTwo(){
	return inputTwo;
}

void Shl::setOutput(string output){
	this->output = output;
}

string Shl::getOutput(){
	return output;
}

void Shl::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Shl::getInputWidth(){
	return Component::getInputWidth();
}

void Shl::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Shl::getOutputWidth(){
	return Component::getOutputWidth();
}

void Shl::setDelay(float delay){
	Component::setDelay(delay);
}

float Shl::getDelay(){
	return Component::getDelay();
}

void Shl::setName(string name){
	Component::setName(name);
}

string Shl::getName(){
	return Component::getName();
}

void Shl::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Shl::getComponentID(){
	return Component::getComponentID();
}

void Shl::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Shl::getComponentType(){
	return Component::getComponentType();
}

void Shl::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Shl::getDataType(){
	return Component::getDataType();
}

void Shl::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Shl::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Shl::generateVerilogExpression(){

}

Shl::~Shl(){

}