#include "Modd.h"

Modd::Modd(){}

Modd::Modd(int inputWidth, int outputWidth, float delay, int componentID) :Component(inputWidth, outputWidth, delay, "MODD", componentID){}

Modd::Modd(int inputWidth, int outputWidth, float delay, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, "Modd", componentID){}

void Modd::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Modd::getInputOne(){
	return inputOne;
}

void Modd::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Modd::getInputTwo(){
	return inputTwo;
}

void Modd::setOutput(string output){
	this->output = output;
}

string Modd::getOutput(){
	return output;
}

void Modd::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Modd::getInputWidth(){
	return Component::getInputWidth();
}

void Modd::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Modd::getOutputWidth(){
	return Component::getOutputWidth();
}

void Modd::setDelay(float delay){
	Component::setDelay(delay);
}

float Modd::getDelay(){
	return Component::getDelay();
}

void Modd::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Modd::getComponentID(){
	return Component::getComponentID();
}

void Modd::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Modd::getComponentType(){
	return Component::getComponentType();
}

void Modd::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Modd::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Modd::generateVerilogExpression(){

}

Modd::~Modd(){

}