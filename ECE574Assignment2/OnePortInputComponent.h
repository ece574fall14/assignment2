#ifndef OnePortInputComponent_H
#define OnePortInputComponent_H
#include <iostream>
#include <string>
using namespace std;
class OnePortInputComponent{
public:
	virtual void setFanInComponent(Component *fanInComponent);
	virtual Component getFanInComponent();
	virtual void putIntoFanOutComponentList(Component *fanOutComponent);
	virtual Component getFromFanOutComponentList(int index);
};
#endif