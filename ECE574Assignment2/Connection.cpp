#include "Connection.h"

Connection::Connection(Component *nextComponent) :nextComponent(nextComponent){}

void Connection::setNextComponent(Component *nextComponent){
	this->nextComponent = nextComponent;
}

Component Connection::getNextComponent(){
	return *nextComponent;
}

Component::~Component(){
	
}