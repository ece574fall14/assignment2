#ifndef TwoPortInputComponent_H
#define TwoPortInputComponent_H
#include <iostream>
#include <string>
using namespace std;

class TwoPortInputComponent{
public:
	virtual void setInputOne(string inputOne);
	virtual string getInputOne();
	virtual void setInputTwo(string inputTwo);
	virtual string getInputTwo();
	virtual void setOutput(string output);
	virtual string getOutput();
};
#endif