#include "Complt.h"

Complt::Complt(){}

Complt::Complt(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "COMPLT", dataType, componentID){
	setName("complt" + componentID);
}

Complt::Complt(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "COMPLT", dataType, componentID){
	setName("complt" + componentID);
}

void Complt::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Complt::getInputOne(){
	return inputOne;
}

void Complt::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Complt::getInputTwo(){
	return inputTwo;
}

void Complt::setOutput(string output){
	this->output = output;
}

string Complt::getOutput(){
	return output;
}

void Complt::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Complt::getInputWidth(){
	return Component::getInputWidth();
}

void Complt::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Complt::getOutputWidth(){
	return Component::getOutputWidth();
}

void Complt::setDelay(float delay){
	Component::setDelay(delay);
}

float Complt::getDelay(){
	return Component::getDelay();
}

void Complt::setName(string name){
	Component::setName(name);
}

string Complt::getName(){
	return Component::getName();
}

void Complt::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Complt::getComponentID(){
	return Component::getComponentID();
}

void Complt::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Complt::getComponentType(){
	return Component::getComponentType();
}

void Complt::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Complt::getDataType(){
	return Component::getDataType();
}


void Complt::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Complt::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Complt::generateVerilogExpression(){

}

Complt::~Complt(){

}