#include "Wire.h"

Wire::Wire(){}

Wire::Wire(string name, int width, string dataType, int componentID) :
name(name), Component(width, width, WIRE_DELAY, name, "WIRE", dataType, componentID){}

void Wire::setName(string name){
	Component::setName(name);
}

string Wire::getName(){
	return Component::getName();
}

void Wire::setFanInComponent(Component *fanInComponent){
	this->fanInComponent = fanInComponent;
}

Component Wire::getFanInComponent(){
	return *fanInComponent;
}

void Wire::putIntoFanOutComponentList(Component *fanOutComponent){
	fanOutComponentList.push_back(fanOutComponent);
}

Component Wire::getFromFanOutComponentList(int index){
	return *(fanOutComponentList.at(index));
}

void Wire::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Wire::getInputWidth(){
	return Component::getInputWidth();
}

void Wire::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Wire::getOutputWidth(){
	return Component::getOutputWidth();
}

void Wire::setDelay(float delay){
	Component::setDelay(delay);
}

float Wire::getDelay(){
	return Component::getDelay();
}

void Wire::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Wire::getComponentID(){
	return Component::getComponentID();
}

void Wire::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Wire::getComponentType(){
	return Component::getComponentType();
}

void Wire::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Wire::getDataType(){
	return Component::getDataType();
}

void Wire::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Wire::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Wire::generateVerilogExpression(){

}

Wire::~Wire(){

}