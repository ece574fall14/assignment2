#ifndef Reg_H
#define Reg_H
#include "Component.h"
#include "ThreePortInputComponent.h"
#include "Constants.h"

class Reg :public Component, public ThreePortInputComponent{
private:
	string inputOne;
	string inputTwo;
	string inputThree;
	string output;

public:
	explicit Reg();
	explicit Reg(int inputWidth, int outputWidth, float delay, string dataType, int componentID);
	explicit Reg(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output);
	void setInputOne(string inputOne);
	string getInputOne();
	void setInputTwo(string inputTwo);
	string getInputTwo();
	void setInputThree(string inputThree);
	string getInputThree();
	void setOutput(string output);
	string getOutput();
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setName(string name);
	string getName();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	void setDataType(string dataType);
	string getDataType();
	virtual void generateVerilogExpression();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	~Reg();
};
#endif