#ifndef VerilogGenerator_H
#define	VerilogGenerator_H
#include <iostream>
#include <vector>
#include "Component.h"
class VerilogGenerator{
public:
	//explicit VerilogGenerator();
	static float computeCriticalPath(const vector<Component> &componentsList);
	static void generateVerilogCode(string outputVerilogFileName, const vector<Component> &portAndWireList, const vector<Component> &componentsList);
	//~VerilogGenerator();
};
#endif // !VerilogGenerator_H
