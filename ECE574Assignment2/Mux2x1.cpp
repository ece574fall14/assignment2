#include "Mux2x1.h"

Mux2x1::Mux2x1(){}

Mux2x1::Mux2x1(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "MUX2X1", dataType, componentID){
	setName("mux" + componentID);
}

Mux2x1::Mux2x1(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "MUX2X1", dataType, componentID){
	setName("mux" + componentID);
}

void Mux2x1::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Mux2x1::getInputOne(){
	return inputOne;
}

void Mux2x1::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Mux2x1::getInputTwo(){
	return inputTwo;
}

void Mux2x1::setInputThree(string inputThree){
	this->inputThree = inputThree;
}

string Mux2x1::getInputThree(){
	return inputThree;
}

void Mux2x1::setOutput(string output){
	this->output = output;
}

string Mux2x1::getOutput(){
	return output;
}

void Mux2x1::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Mux2x1::getInputWidth(){
	return Component::getInputWidth();
}

void Mux2x1::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Mux2x1::getOutputWidth(){
	return Component::getOutputWidth();
}

void Mux2x1::setDelay(float delay){
	Component::setDelay(delay);
}

float Mux2x1::getDelay(){
	return Component::getDelay();
}

void Mux2x1::setName(string name){
	Component::setName(name);
}

string Mux2x1::getName(){
	return Component::getName();
}


void Mux2x1::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Mux2x1::getComponentID(){
	return Component::getComponentID();
}

void Mux2x1::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Mux2x1::getComponentType(){
	return Component::getComponentType();
}

void Mux2x1::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Mux2x1::getDataType(){
	return Component::getDataType();
}

void Mux2x1::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Mux2x1::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Mux2x1::generateVerilogExpression(){

}

Mux2x1::~Mux2x1(){

}