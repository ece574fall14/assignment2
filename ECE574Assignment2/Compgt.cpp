#include "Compgt.h"

Compgt::Compgt(){}

Compgt::Compgt(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "COMPGT", dataType, componentID){
	setName("compgt" + componentID);
}

Compgt::Compgt(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "COMPGT", dataType, componentID){
	setName("compgt" + componentID);
}

void Compgt::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Compgt::getInputOne(){
	return inputOne;
}

void Compgt::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Compgt::getInputTwo(){
	return inputTwo;
}

void Compgt::setOutput(string output){
	this->output = output;
}

string Compgt::getOutput(){
	return output;
}

void Compgt::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Compgt::getInputWidth(){
	return Component::getInputWidth();
}

void Compgt::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Compgt::getOutputWidth(){
	return Component::getOutputWidth();
}

void Compgt::setDelay(float delay){
	Component::setDelay(delay);
}

float Compgt::getDelay(){
	return Component::getDelay();
}

void Compgt::setName(string name){
	Component::setName(name);
}

string Compgt::getName(){
	return Component::getName();
}

void Compgt::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Compgt::getComponentID(){
	return Component::getComponentID();
}

void Compgt::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Compgt::getComponentType(){
	return Component::getComponentType();
}

void Compgt::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Compgt::getDataType(){
	return Component::getDataType();
}

void Compgt::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Compgt::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Compgt::generateVerilogExpression(){

}

Compgt::~Compgt(){

}