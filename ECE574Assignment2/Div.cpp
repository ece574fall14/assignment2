#include "Div.h"

Div::Div(){}

Div::Div(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "DIV", dataType, componentID){
	setName("div" + componentID);
}

Div::Div(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "DIV", dataType, componentID){
	setName("div" + componentID);
}

void Div::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Div::getInputOne(){
	return inputOne;
}

void Div::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Div::getInputTwo(){
	return inputTwo;
}

void Div::setOutput(string output){
	this->output = output;
}

string Div::getOutput(){
	return output;
}

void Div::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Div::getInputWidth(){
	return Component::getInputWidth();
}

void Div::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Div::getOutputWidth(){
	return Component::getOutputWidth();
}

void Div::setDelay(float delay){
	Component::setDelay(delay);
}

float Div::getDelay(){
	return Component::getDelay();
}

void Div::setName(string name){
	Component::setName(name);
}

string Div::getName(){
	return Component::getName();
}

void Div::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Div::getComponentID(){
	return Component::getComponentID();
}

void Div::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Div::getComponentType(){
	return Component::getComponentType();
}

void Div::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Div::getDataType(){
	return Component::getDataType();
}

void Div::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Div::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Div::generateVerilogExpression(){

}

Div::~Div(){

}