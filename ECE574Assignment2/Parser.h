#ifndef Parser_H
#define	Parser_H
#include <iostream>
#include <fstream>
#include <vector>
//#include <regex>
#include <string>
#include "Component.h"
#include "Add.h"
#include "Compeq.h"
#include "Compgt.h"
#include "Complt.h"
#include "Div.h"
#include "Mul.h"
#include "Mux2x1.h"
#include "Port.h"
#include "Reg.h"
#include "Shl.h"
#include "Shr.h"
#include "Sub.h"
#include "Wire.h"
using namespace std;

class Parser{
private:
	static void buildComponents(vector<string> tokens, vector<Component *> &wireList, vector<Component *> &componentsList, int componentID);
public:
	//explicit Parser();
	//explicit Parser(const FILE &inputFile, const vector<Component> &portAndWireList, const vector<Component> &componentsList);
	static void parsingText(string inputCircuitFileName, vector<Component *> &wireList,  vector<Component *> &componentsList);
	static void generateGraph(vector<Component *> &wireList, vector<Component *> &componentsList);
	//~Parser();
};
#endif // !Parser_H
