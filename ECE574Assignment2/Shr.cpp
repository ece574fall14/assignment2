#include "Shr.h"

Shr::Shr(){}

Shr::Shr(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "SHR", dataType, componentID){
	setName("shr" + componentID);
}

Shr::Shr(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "SHR", dataType, componentID){
	setName("shr" + componentID);
}
void Shr::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Shr::getInputOne(){
	return inputOne;
}

void Shr::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Shr::getInputTwo(){
	return inputTwo;
}

void Shr::setOutput(string output){
	this->output = output;
}

string Shr::getOutput(){
	return output;
}

void Shr::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Shr::getInputWidth(){
	return Component::getInputWidth();
}

void Shr::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Shr::getOutputWidth(){
	return Component::getOutputWidth();
}

void Shr::setDelay(float delay){
	Component::setDelay(delay);
}

float Shr::getDelay(){
	return Component::getDelay();
}

void Shr::setName(string name){
	Component::setName(name);
}

string Shr::getName(){
	return Component::getName();
}

void Shr::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Shr::getComponentID(){
	return Component::getComponentID();
}

void Shr::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Shr::getComponentType(){
	return Component::getComponentType();
}

void Shr::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Shr::getDataType(){
	return Component::getDataType();
}

void Shr::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Shr::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Shr::generateVerilogExpression(){

}

Shr::~Shr(){

}