#include "Parser.h"

void Parser::parsingText(string inputCircuitFileName, vector<Component *> &wireList, vector<Component *> &componentsList){
	fstream inputStream;
	string lineBuff;
	vector<char> token;
	vector<string> tokens;
	int componentID = 0;
	inputStream.open(inputCircuitFileName, ios::in);
	while (!inputStream.eof()){
		getline(inputStream, lineBuff);
		int i = 0;
		while (i != lineBuff.size()){
			if (lineBuff[i] == ' ' || lineBuff[i] == '\t' || lineBuff[i] == ','){
				if (!token.empty()){
					tokens.push_back(string(token.begin(), token.end()));
				}
				token.clear();
			}
			else{
				token.push_back(lineBuff[i]);
			}
		}
		buildComponents(tokens, wireList, componentsList, componentID);
		componentID++;
	}
}

/*void Parser::parsingText(string inputCircuitFileName, const vector<Component> &portAndWireList, const vector<Component> &componentsList){
vector<regex> patterns;
regex pattern();
}*/

void Parser::generateGraph(vector<Component *> &wireList, vector<Component *> &componentsList){

}

void Parser::buildComponents(vector<string> tokens, vector<Component *> &wireList, vector<Component *> &componentsList, int componentID){
	int inputWidth;
	int outputWidth;
	string dataType;
	string componentType;
	Component *tempComponent;
	if (tokens.at(0) == "input"){
		componentType = "INPUT_PORT";
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Port(tokens.at(i), inputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "output"){
		componentType = "OUTPUT_PORT";
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Port(tokens.at(i), inputWidth, "OUTPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "wire"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Wire(tokens.at(i), inputWidth, dataType, componentID);
			wireList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "register"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Reg(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	//////added
	else if (tokens.at(0) == "wire"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Wire(tokens.at(i), inputWidth, dataType, componentID);
			wireList.push_back(tempComponent);
		}
	}
	//REG ADD SUB COMPGT COMPLT COMPEQ DIV MUL MUX2X1 SHL SHR MOD
	else if (tokens.at(0) == "sub"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Sub(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	/////////////////
	else if (tokens.at(0) == "add"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Add(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "compgt"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Compgt(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "complt"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Complt(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "compeq"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Compeq(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "div"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Div(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "mul"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Mul(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "mux2x1"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Mux2x1(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "shl"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Shl(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "shr"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Shr(tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}
	else if (tokens.at(0) == "modd"){
		if (tokens.at(1) == "Int1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "INT";
		}
		if (tokens.at(1) == "Int64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt1"){
			inputWidth = 1;
			outputWidth = 1;
			dataType = "INT";
		}
		if (tokens.at(1) == "UInt8"){
			inputWidth = 8;
			outputWidth = 8;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt16"){
			inputWidth = 16;
			outputWidth = 16;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt32"){
			inputWidth = 32;
			outputWidth = 32;
			dataType = "UINT";
		}
		if (tokens.at(1) == "UInt64"){
			inputWidth = 64;
			outputWidth = 64;
			dataType = "UINT";
		}
		int i = 2;
		while (i != tokens.size()){
			tempComponent = new Modd (tokens.at(i), inputWidth, outputWidth, "INPUT", dataType, componentID);
			componentsList.push_back(tempComponent);
		}
	}

}