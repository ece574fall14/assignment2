#ifndef Port_H
#define Port_H
#include <vector>
#include <string>
#include "Component.h"
#include "OnePortInputComponent.h"
#include "Constants.h"

class Port :public Component, public OnePortInputComponent{
private:
	string portType;

public:
	explicit Port();
	explicit Port(string name, int width, string portType, string dataType, int componentID);
	void setName(string name);
	string getName();
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	void setDataType(string dataType);
	string getDataType();
	void setPortType(string portType);
	string getPortType();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	void generateVerilogExpression();
	~Port();
};
#endif