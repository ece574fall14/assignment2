#include "Compeq.h"

Compeq::Compeq(){}

Compeq::Compeq(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "COMPEQ", dataType, componentID){
	setName("compeq" + componentID);
}

Compeq::Compeq(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "COMPEQ", dataType, componentID){
	setName("compeq" + componentID);
}

void Compeq::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Compeq::getInputOne(){
	return inputOne;
}

void Compeq::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Compeq::getInputTwo(){
	return inputTwo;
}

void Compeq::setOutput(string output){
	this->output = output;
}

string Compeq::getOutput(){
	return output;
}

void Compeq::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Compeq::getInputWidth(){
	return Component::getInputWidth();
}

void Compeq::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Compeq::getOutputWidth(){
	return Component::getOutputWidth();
}

void Compeq::setDelay(float delay){
	Component::setDelay(delay);
}

float Compeq::getDelay(){
	return Component::getDelay();
}

void Compeq::setName(string name){
	Component::setName(name);
}

string Compeq::getName(){
	return Component::getName();
}

void Compeq::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Compeq::getComponentID(){
	return Component::getComponentID();
}

void Compeq::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Compeq::getComponentType(){
	return Component::getComponentType();
}

void Compeq::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Compeq::getDataType(){
	return Component::getDataType();
}


void Compeq::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Compeq::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Compeq::generateVerilogExpression(){

}

Compeq::~Compeq(){

}