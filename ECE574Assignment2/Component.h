#ifndef Component_H
#define Component_H

#include <iostream>
#include <string>
#include <vector>
#include "Connection.h"
using namespace std;

class Component{
private:
	int inputWidth;
	int outputWidth;
	float delay;
	string name;
	int componentID;
	string verilogExpression;
	string componentType;
	string dataType;
	vector<Connection> *connectionList;
public:
	explicit Component(void);
	explicit Component(int inputWidth, int outputWidth, float delay, string name, string componentType, string dataType, int componentID);
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setName(string name);
	string getName();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	void setDataType(string dataType);
	string getDataType();
	virtual void generateVerilogExpression();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	void outputVerilogExpression();
	~Component();
};
#endif