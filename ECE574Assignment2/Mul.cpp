#include "Mul.h"

Mul::Mul(){}

Mul::Mul(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "MUL", dataType, componentID){
	setName("mul" + componentID);
}

Mul::Mul(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "MUL", dataType, componentID){
	setName("mul" + componentID);
}

void Mul::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Mul::getInputOne(){
	return inputOne;
}

void Mul::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Mul::getInputTwo(){
	return inputTwo;
}

void Mul::setOutput(string output){
	this->output = output;
}

string Mul::getOutput(){
	return output;
}

void Mul::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Mul::getInputWidth(){
	return Component::getInputWidth();
}

void Mul::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Mul::getOutputWidth(){
	return Component::getOutputWidth();
}

void Mul::setDelay(float delay){
	Component::setDelay(delay);
}

float Mul::getDelay(){
	return Component::getDelay();
}

void Mul::setName(string name){
	Component::setName(name);
}

string Mul::getName(){
	return Component::getName();
}

void Mul::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Mul::getComponentID(){
	return Component::getComponentID();
}

void Mul::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Mul::getComponentType(){
	return Component::getComponentType();
}

void Mul::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Mul::getDataType(){
	return Component::getDataType();
}

void Mul::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Mul::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Mul::generateVerilogExpression(){

}

Mul::~Mul(){

}