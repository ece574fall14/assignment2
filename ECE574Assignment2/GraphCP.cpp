#include "Component.h"
#include "Connection.h"
#include <queue>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

#define MAX 10000
#define numv 20
#define nume 20
#define MAX_VERTEX_NUM 20
int ve [MAX_VERTEX_NUM]; 
#define Stack_Size 100

class sqStack
{
public:
	int *element;
	int top;
	int stackSize;  //size of stack
};

void initStack_Sq(sqStack &S)
{
	S.element = new int[Stack_Size];
	S.top = -1;
	S.stackSize = Stack_Size;
}


void push(sqStack &S, int x)
{
	if (S.top == Stack_Size - 1)
		cout << "Stack Overflow!";
	S.element[++S.top] = x;
}


int pop(sqStack &S)
{
	int x;
	if (S.top == -1)
		cout << "Stack Empty!";
	x = S.element[S.top--];
	return x;
}

class Edgenode
{
public:
	int adjvex;
	class Edgenode *nextedge;
	int weight;
};

class Vexnode
{
public:
	char vex;
	Edgenode * firstedge;
	int indegree;
};

class Graph
{
public:
	Vexnode vexs[MAX_VERTEX_NUM];
	int vexnum;
	int edgenum;
};

void CreateGraph(Graph &G, int n, int e) ///n means the number of vertex, e means the number of edges
{
	int i, j, k, w;
	G.vexnum = n;
	G.edgenum = e;
	for (i = 0; i < n; i++)
	{
		cin >> G.vexs[i].vex;
		G.vexs[i].firstedge = NULL;
	}

	for (k = 0; k < e; k++)
	{
		Edgenode *P = new Edgenode;
		cin >> i >> j >> w;
		p->adjvex = j;
		p->weight = w;
		p->nextedge = G.vexs[i].firstedge;
		G.vexs[i].firstedge = p;
	}
}

void Sorting(Graph &G, sqStack &T)
{
	sqStack S;
	initStack_Sq(S);
	Edgenode *p;

	int count = 0;
	int i;
	for (i = 0; i < G.vexnum; i++)
		G.vexs[i].indegree = 0;
	for (i = 0; i < G.vexnum; i++)
	{//calculate the indegree of each vertex
		p = G.vexs[i].firstedge;
		while (p)
		{
			G.vexs[p->adjvex].indegree++;
			p = p->nextedge;
		}

	}
	for (i = 0; i < G.vexnum; i++)
	if (G.vexs[i].indegree == 0)
		push(S, i);	
	for (i = 0; i < G.vexnum; i++)
		ve[i] = 0; //initialized start time 
	while (S.top != -1)
	{
		i = pop(S);
		cout << G.vexs[i].vex << " ";
		push(T, i);
		count++;
		p = G.vexs[i].firstedge;
		while (p)
		{
			int k;
			int dut;
			dut = p->weight;
			k = p->adjvex;
			G.vexs[k].indegree--;
			if (G.vexs[k].indegree == 0)
				push(S, k);
			if (ve[i] + dut > ve[k])
				ve[k] = ve[i] + dut;
			p = p->nextedge;
		}
	}
	cout <<endl;
	if (count < G.vexnum)
		cout << "The orderings of Graph G is completed !" << endl;
}

void CriticalPath(Graph &G)
{
	int i, j, k, dut;
	int early_time, late_time;
	int vl[MAX_VERTEX_NUM];
	Edgenode *p;
	sqStack T;
	initStack_Sq(T);
	Sorting(G, T);
	for (i = 0; i<G.vexnum; i++)
		vl[i] = ve[G.vexnum - 1]; 
	
	while (T.top != -1)
	{
		for (j = pop(T), p = G.vexs[j].firstedge; p; p = p->nextedge)
		{ 
			k = p->adjvex;
			dut = p->weight;
			if (vl[k] - dut<vl[j])
				vl[j] = vl[k] - dut; 
			                           
		}
	}
	for (i = 0; i<G.vexnum; i++)
	{//go through all the activities  
		for (p = G.vexs[i].firstedge; p; p = p->nextedge)
		{
			k = p->adjvex;
			dut = p->weight;
			early_time = ve[i];//find out the earliest time of one activities
			late_time = vl[k] - dut;//find out the latest time of one activities  
			if (early_time == late_time)
			{//if two values are the same, then it indicating that this event is the critical activities 
				cout << "(" << G.vexs[i].vex << "," << G.vexs[k].vex << ")" << dut << " ";
				cout << "ee=" << early_time << "," << "el=" << late_time << endl;
			}
		}
	}
}


void main()
{
	freopen("filename.txt", "r", stdin);
	Graph G;
	CreateGraph(G, numv, nume);
	CriticalPath(G);
}






