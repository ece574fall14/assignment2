#include "Component.h"

Component::Component(){}

Component::Component(int inputWidth, int outputWidth, float delay, string name, string componentType, string dataType, int componentID):
inputWidth(inputWidth),
outputWidth(outputWidth),
delay(delay),
name(name),
componentType(componentType),
dataType(dataType),
componentID(componentID){}

void Component::setInputWidth(int inputWidth){
	this->inputWidth = inputWidth;
}

int Component::getInputWidth(){
	return inputWidth;
}

void Component::setOutputWidth(int outputWidth){
	this->outputWidth = outputWidth;
}

int Component::getOutputWidth(){
	return outputWidth;
}

void Component::setDelay(float delay){
	this->delay = delay;
}

float Component::getDelay(){
	return delay;
}

void Component::setName(string name){
	this->name;
}

string Component::getName(){
	return name;
}

void Component::setComponentID(int componentID){
	this->componentID = componentID;
}

int Component::getComponentID(){
	return componentID;
}

void Component::setComponentType(string componentType){
	this->componentType = componentType;
}

string Component::getComponentType(){
	return componentType;
}

void Component::setDataType(string dataType){
	this->dataType = dataType;
}

string Component::getDataType(){
	return dataType;
}

void Component::setVerilogExpression(string verilogExpression){
	this->verilogExpression = verilogExpression;
}

string Component::getVerilogExpression(){
	return verilogExpression;
}

void Component::outputVerilogExpression(){
	cout << verilogExpression << endl;
}

Component::~Component(){

}