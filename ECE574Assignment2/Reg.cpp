#include "Reg.h"

Reg::Reg(){}

Reg::Reg(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "REG", dataType, componentID){
	setName("reg" + componentID);
}

Reg::Reg(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "REG", dataType, componentID){
	setName("reg" + componentID);
}

void Reg::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Reg::getInputOne(){
	return inputOne;
}

void Reg::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Reg::getInputTwo(){
	return inputTwo;
}

void Reg::setInputThree(string inputThree){
	this->inputThree = inputThree;
}

string Reg::getInputThree(){
	return inputThree;
}

void Reg::setOutput(string output){
	this->output = output;
}

string Reg::getOutput(){
	return output;
}

void Reg::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Reg::getInputWidth(){
	return Component::getInputWidth();
}

void Reg::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Reg::getOutputWidth(){
	return Component::getOutputWidth();
}

void Reg::setDelay(float delay){
	Component::setDelay(delay);
}

float Reg::getDelay(){
	return Component::getDelay();
}

void Reg::setName(string name){
	Component::setName(name);
}

string Reg::getName(){
	return Component::getName();
}

void Reg::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Reg::getComponentID(){
	return Component::getComponentID();
}

void Reg::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Reg::getComponentType(){
	return Component::getComponentType();
}

void Reg::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Reg::getDataType(){
	return Component::getDataType();
}

void Reg::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Reg::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Reg::generateVerilogExpression(){

}

Reg::~Reg(){

}