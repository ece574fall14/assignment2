#ifndef Add_H
#define Add_H
#include "Component.h"
#include "TwoPortInputComponent.h"
#include "Constants.h"

class Add :public Component, public TwoPortInputComponent{
private:
	string inputOne;
	string inputTwo;
	string output;

public:
	explicit Add();
	explicit Add(int inputWidth, int outputWidth, float delay, string dataType, int componentID);
	explicit Add(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output);
	void setInputOne(string inputOne);
	string getInputOne();
	void setInputTwo(string inputTwo);
	string getInputTwo();
	void setOutput(string output);
	string getOutput();
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setName(string name);
	string getName();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	void setDataType(string dataType);
	string getDataType();
	virtual void generateVerilogExpression();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	~Add();
};
#endif