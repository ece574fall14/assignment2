#ifndef ThreePortInputComponent_H
#define ThreePortInputComponent_H
#include <iostream>
#include <string>
using namespace std;

class ThreePortInputComponent{
public:
	virtual void setInputOne(string inputOne);
	virtual string getInputOne();
	virtual void setInputTwo(string inputTwo);
	virtual string getInputTwo();
	virtual void setInputThree(string inputThree);
	virtual string getInputThree();
	virtual void setOutput(string output);
	virtual string getOutput();
};
#endif