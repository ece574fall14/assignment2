#include "Sub.h"

Sub::Sub(){}

Sub::Sub(int inputWidth, int outputWidth, float delay, string dataType, int componentID) :Component(inputWidth, outputWidth, delay, NULL, "SUB", dataType, componentID){
	setName("sub" + componentID);
}

Sub::Sub(int inputWidth, int outputWidth, float delay, string dataType, int componentID, string inputOne, string inputTwo, string output) : inputOne(inputOne), inputTwo(inputTwo), output(output), Component(inputWidth, outputWidth, delay, NULL, "SUB", dataType, componentID){
	setName("sub" + componentID);
}

void Sub::setInputOne(string inputOne){
	this->inputOne = inputOne;
}

string Sub::getInputOne(){
	return inputOne;
}

void Sub::setInputTwo(string inputTwo){
	this->inputTwo = inputTwo;
}

string Sub::getInputTwo(){
	return inputTwo;
}

void Sub::setOutput(string output){
	this->output = output;
}

string Sub::getOutput(){
	return output;
}

void Sub::setInputWidth(int inputWidth){
	Component::setInputWidth(inputWidth);
}

int Sub::getInputWidth(){
	return Component::getInputWidth();
}

void Sub::setOutputWidth(int outputWidth){
	Component::setOutputWidth(outputWidth);
}

int Sub::getOutputWidth(){
	return Component::getOutputWidth();
}

void Sub::setDelay(float delay){
	Component::setDelay(delay);
}

float Sub::getDelay(){
	return Component::getDelay();
}

void Sub::setName(string name){
	Component::setName(name);
}

string Sub::getName(){
	return Component::getName();
}

void Sub::setComponentID(int componentID){
	Component::setComponentID(componentID);
}

int Sub::getComponentID(){
	return Component::getComponentID();
}

void Sub::setComponentType(string componentType){
	Component::setComponentType(componentType);
}

string Sub::getComponentType(){
	return Component::getComponentType();
}

void Sub::setDataType(string dataType){
	Component::setDataType(dataType);
}

string Sub::getDataType(){
	return Component::getDataType();
}

void Sub::setVerilogExpression(string verilogExpression){
	Component::setVerilogExpression(verilogExpression);
}

string Sub::getVerilogExpression(){
	return Component::getVerilogExpression();
}

void Sub::generateVerilogExpression(){

}

Sub::~Sub(){

}