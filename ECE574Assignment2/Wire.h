#ifndef Wire_H
#define Wire_H
#include <vector>
#include <string>
#include "Component.h"
#include "OnePortInputComponent.h"
#include "Constants.h"

class Wire :public Component, public OnePortInputComponent{
private:
	string name;
	Component *fanInComponent = NULL;
	vector<Component *> fanOutComponentList;

public:
	explicit Wire();
	explicit Wire(string name, int width, string dataType, int componentID);
	void setName(string name);
	string getName();
	void setFanInComponent(Component *fanInComponent);
	Component getFanInComponent();
	void putIntoFanOutComponentList(Component *fanOutComponent);
	Component getFromFanOutComponentList(int index);
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	void setDataType(string dataType);
	string getDataType();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	void generateVerilogExpression();
	~Wire();
};
#endif