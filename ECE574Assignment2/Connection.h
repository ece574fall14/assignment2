#ifndef Connection_H
#define Connection_H
#include "Component.h"

class Connection{
private:
	Component *nextComponent;
public:
	explicit Connection(const Component *nextComponent);
	void setNextComponent(const Component *nextComponent);
	Component getNextComponent();
	~Connection();
};
#endif