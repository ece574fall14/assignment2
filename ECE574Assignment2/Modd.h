#ifndef Modd_H
#define Modd_H
#include "Component.h"
#include "TwoPortInputComponent.h"
#include "Constants.h"

class Modd :public Component, public TwoPortInputComponent{
private:
	string inputOne;
	string inputTwo;
	string output;

public:
	Modd();
	Modd(int inputWidth, int outputWidth, float delay, int componentID);
	Modd(int inputWidth, int outputWidth, float delay, int componentID, string inputOne, string inputTwo, string output);
	void setInputOne(string inputOne);
	string getInputOne();
	void setInputTwo(string inputTwo);
	string getInputTwo();
	void setOutput(string output);
	string getOutput();
	void setInputWidth(int inputWidth);
	int getInputWidth();
	void setOutputWidth(int outputWidth);
	int getOutputWidth();
	void setDelay(float delay);
	float getDelay();
	void setComponentID(int componentID);
	int getComponentID();
	void setComponentType(string componentType);
	string getComponentType();
	virtual void generateVerilogExpression();
	void setVerilogExpression(string verilogExpression);
	string getVerilogExpression();
	~Modd();
};
#endif